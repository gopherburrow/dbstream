package dbstream

import (
	"context"
	"database/sql"
	"log"
)

type RowReaderFunc func() (item interface{}, scanArgs []interface{})

func DbQueryContext(ctx context.Context, db *sql.DB, query string, rr RowReaderFunc, args ...interface{}) (chan interface{}, error) {
	rows, err := db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	return rowsScanContext(ctx, rows, rr)
}

func DbQuery(db *sql.DB, query string, rr RowReaderFunc, args ...interface{}) (chan interface{}, error) {
	rows, err := db.Query(query, args...)
	if err != nil {
		return nil, err
	}
	dataChan, err := rowsScan(rows, rr)
	return dataChan, err
}

func ConnQueryContext(ctx context.Context, conn *sql.Conn, query string, rr RowReaderFunc, args ...interface{}) (chan interface{}, error) {
	rows, err := conn.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	return rowsScanContext(ctx, rows, rr)
}

func TxQueryContext(ctx context.Context, tx *sql.Tx, query string, rr RowReaderFunc, args ...interface{}) (chan interface{}, error) {
	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	return rowsScanContext(ctx, rows, rr)
}

func TxQuery(tx *sql.Tx, query string, rr RowReaderFunc, args ...interface{}) (chan interface{}, error) {
	rows, err := tx.Query(query, args...)
	if err != nil {
		return nil, err
	}
	dataChan, err := rowsScan(rows, rr)
	return dataChan, err
}

func StmtQueryContext(ctx context.Context, stmt *sql.Stmt, rr RowReaderFunc, args ...interface{}) (chan interface{}, error) {
	rows, err := stmt.QueryContext(ctx, args...)
	if err != nil {
		return nil, err
	}
	return rowsScanContext(ctx, rows, rr)
}

func StmtQuery(stmt *sql.Stmt, rr RowReaderFunc, args ...interface{}) (chan interface{}, error) {
	rows, err := stmt.Query(args...)
	if err != nil {
		return nil, err
	}
	dataChan, err := rowsScan(rows, rr)
	return dataChan, err
}

func firstRowScan(rows *sql.Rows, rr RowReaderFunc) (value interface{}, ok bool, err error) {
	if rows.Next() {
		v, scanArgs := rr()
		if err := rows.Scan(scanArgs...); err != nil {
			return nil, false, err
		}
		return v, true, nil
	}
	if err := rows.Close(); err != nil {
		log.Println(err)
	}
	if err := rows.Err(); err != nil {
		return nil, false, err
	}
	return nil, false, nil
}

func rowsScanContext(ctx context.Context, rows *sql.Rows, rr RowReaderFunc) (chan interface{}, error) {
	v, ok, err := firstRowScan(rows, rr)
	if err != nil {
		return nil, err
	}
	dataChan := make(chan interface{})
	if !ok {
		close(dataChan)
		return dataChan, nil
	}

	go func() {
		defer rows.Close()
		defer close(dataChan)

		dataChan <- v

		for rows.Next() {
			v, scanArgs := rr()
			if err := rows.Scan(scanArgs...); err != nil {
				dataChan <- err
				return
			}
			select {
			case dataChan <- v:
			case <-ctx.Done():
				dataChan <- ctx.Err()
				return
			}
		}
		if err := rows.Err(); err != nil {
			dataChan <- err
			return
		}
	}()
	return dataChan, nil
}

func rowsScan(rows *sql.Rows, rr RowReaderFunc) (chan interface{}, error) {
	v, ok, err := firstRowScan(rows, rr)
	if err != nil {
		return nil, err
	}
	dataChan := make(chan interface{})
	if !ok {
		close(dataChan)
		return dataChan, nil
	}

	go func() {
		defer rows.Close()
		defer close(dataChan)

		dataChan <- v

		for rows.Next() {
			v, scanArgs := rr()
			if err := rows.Scan(scanArgs...); err != nil {
				dataChan <- err
				return
			}
			dataChan <- v
		}
		if err := rows.Err(); err != nil {
			dataChan <- err
			return
		}
	}()
	return dataChan, nil
}
